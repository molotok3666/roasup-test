<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Goods\Dto\GetGoodsDto;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "get_goods": {
 *              "method": "POST",
 *              "path"="/goods/get-item",
 *              "input"="App\Goods\Dto\GetGoodsDto"
 *         }
 *     },
 *     itemOperations={
 *         "get"
 *     }
 * )
 *
 * @ORM\Table(name="goods")
 * @ORM\Entity
 */
class Goods
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups("goodsId")
     */
    private int $id;

    /**
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private string $title;

    /**
     * @ORM\Column(name="category", type="string", nullable=false)
     */
    private string $category;

    /**
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private int $price;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(): int
    {
        return $this->id;
    }
}
