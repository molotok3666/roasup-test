<?php

declare(strict_types=1);

namespace App\Goods\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Entity\Goods;
use App\Goods\Dto\GetGoodsDto;
use Doctrine\ORM\EntityManagerInterface;

final class GetGoodsDataTransformer implements DataTransformerInterface
{
    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $goodsRepo = $this->em->getRepository(Goods::class);

        // сделать 404 если не нашли
        return $goodsRepo->find($data->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return GetGoodsDto::class === $to && 'get_goods' == ($context['collection_operation_name'] ?? '');
    }
}
