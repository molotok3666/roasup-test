<?php
declare(strict_types=1);

namespace App\Goods\Dto;

class GetGoodsDto
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(): int
    {
        return $this->id;
    }
}